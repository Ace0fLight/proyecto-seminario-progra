#include <iostream>
#include <cmath>
#include <string>
using namespace std;

struct variables{
    double *exponente;
    double *constante; 
    double *exponente_c;
    double *constante_c;
};

variables getExponenteConstante(int n,int n2, int boton){ //"0" de "no cociente" y "1" de "si cociente"
	variables v;
    v.exponente = new double[n];
    v.constante = new double[n];
    v.exponente_c = new double[n2];
    v.constante_c = new double[n2];
	switch(boton){
        case 0:
            int i;
            for (i = 0; i < n; i++){
                cout << "Ingrese el Exponente de la variable X: " << endl;
                cin >> v.exponente[i];
                cout << "Ingrese el valor de la constante: " << endl;
                cin >> v.constante[i];
            }
            break;
        case 1:
            cout << "Numerador" << endl;
            int ii;
            for (ii = 0; ii < n; ii++){
                cout << "Ingrese el Exponente de la variable X: " << endl;
                cin >> v.exponente[ii];
                cout << "Ingrese el valor de la constante: " << endl;
                cin >> v.constante[ii];
            }
            cout << "Denominador" << endl;

            for(int j = 0; j < n2; j++){
                cout << "Ingrese el Exponente de la variable X: " << endl;
                cin >> v.exponente_c[j];
                cout << "Ingrese el valor de la constante: " << endl;
                cin >> v.constante_c[j];
            
            }
            break;
    }
    return v;
}

// Y = [f´(x1)*X] [-f´(x1)*X1 + f(x1)] = mx + b
//derivada
double pendiente(double x,int n, variables b){

    double contador = 0;
    for(int i = 0; i < n; i++){
        if(x == 0 && (b.exponente[i] == 1 || b.exponente[i] == 0)){
            contador += 0;
        }else{
            contador += b.constante[i]*b.exponente[i]*pow(x,b.exponente[i]-1);
        }
    }
    return contador;
}

double ultimate(double ubicacion,int camino, variables bloque){
    double elmensajero = 0;
    for(int registro = 0; registro < camino; registro++){
        if(ubicacion == 0 && (bloque.exponente[registro] == 1 || bloque.exponente[registro] == 0)){
            elmensajero -= bloque.constante[registro]*bloque.exponente[registro]; //dev
            elmensajero += bloque.constante[registro]; //normal            
        }else{
            elmensajero -= ubicacion*bloque.constante[registro]*bloque.exponente[registro]*pow(ubicacion,bloque.exponente[registro]-1); //dev
            elmensajero += bloque.constante[registro]*pow(ubicacion,bloque.exponente[registro]); //normal
        }
    }
    return elmensajero;
}

double cociente(double x, int n1, int n2, variables estructura){
    double f = 0,fd = 0,g = 0,gd = 0;
    double retorno; 
    for(int i = 0; i < n1; i++){
        f += estructura.constante[i]*pow(x,estructura.exponente[i]);

        if(x == 0 && (estructura.exponente[i] == 1 || estructura.exponente[i] == 0)){
            fd += 0;
        }else{
            fd += estructura.constante[i]*estructura.exponente[i]*pow(x,estructura.exponente[i]-1);
        }
    }

    for(int t = 0; t < n2; t++){
        g += estructura.constante_c[t]*pow(x,estructura.exponente_c[t]);

        if(x == 0 && (estructura.exponente_c[t] == 1 || estructura.exponente_c[t] == 0)){
            gd += 0;
        }else{
            gd += estructura.constante_c[t]*estructura.exponente_c[t]*pow(x,estructura.exponente_c[t]-1);
        }
    }
    retorno = (fd*g - gd*f)/pow(g,2);
    return retorno;
}

double compcoc(double x, int n1, int n2, variables estructura){

    double parte_1 = cociente(x,n1,n2,estructura)*(-1)*x;
    double cx = 0;
    double xc = 0;
    double parte_2;
    for(int z = 0; z < n1; z++){
        if(x == 0 && (estructura.exponente[z] == 0)){
            cx += estructura.constante[z];
        }else{
            cx += estructura.constante[z]*pow(x,estructura.exponente[z]);
        }
    }
    for(int y = 0; y < n2; y++){
        if(x == 0 && (estructura.exponente_c[y] == 0)){
            xc += estructura.constante_c[y];
        }else{
            xc += estructura.constante_c[y]*pow(x,estructura.exponente_c[y]);
        }
    }
    parte_2 = (cx/xc) + parte_1;
    return parte_2;
}

int main(){
    int xd, xd2, opc;
    double coordenada_x;
  
  	cout << "Bienvenidos a la Calculadora de Derivadas orientadas a funciones made in USM" << endl;
  	cout << "Porfavor ingrese una opcion" << endl; 
		cout << "Opcion 1: Recta Tangente de la Funcion " << endl;
    cout << "Opcion 2: Recta Tangente con cociente" << endl;
  	cout << "Opcion 3: La Derivada de la Funcion Corriente" << endl;
  	cout << "Opcion 4: Salir." << endl;
  	cin >> opc;
  
  	switch(opc){
		case 1:
        {
            cout << "Ingrese cantidad de elementos de f(x): " << endl;
            cin >> xd;
            variables vv = getExponenteConstante(xd,0,0);

            cout << "Ingrese coordenada x: ";
            cin >> coordenada_x;

            if(ultimate(coordenada_x,xd,vv) < 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl; 
                cout <<"F(x): " << pendiente(coordenada_x,xd,vv) <<"x " << ultimate(coordenada_x,xd,vv) << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendiente(coordenada_x,xd,vv) + ultimate(coordenada_x,xd,vv) <<")"<< endl;
            }else if(ultimate(coordenada_x,xd,vv) == 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl; 
                cout <<"F(x): " << pendiente(coordenada_x,xd,vv) <<"x" << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendiente(coordenada_x,xd,vv) + ultimate(coordenada_x,xd,vv) <<")"<< endl;
            }else if(ultimate(coordenada_x,xd,vv) > 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl; 
                cout <<"F(x): " << pendiente(coordenada_x,xd,vv) <<"x +" << ultimate(coordenada_x,xd,vv) << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendiente(coordenada_x,xd,vv) + ultimate(coordenada_x,xd,vv) <<")"<< endl;
            }else{
                cout << "Valor indeterminado" << endl;
            }

          	delete[] vv.exponente; //delete[] (nombre del exponente) para borrar array.
         	delete[] vv.constante;
          	delete[] vv.exponente_c; //delete[] (nombre del exponente) para borrar array.
         	delete[] vv.constante_c;
        	break;
        }     	
      	case 2:
        {
        	cout << "Ingrese cantidad de elementos del numerador de f(x): " << endl;
            cin >> xd;
        	cout << "Ingrese cantidad de elementos del denominador de f(x): " << endl;
            cin >> xd2;
            variables vv1 = getExponenteConstante(xd,xd2,1);

            cout << "Ingrese coordenada x: ";
            cin >> coordenada_x;
            double pendc = cociente(coordenada_x, xd, xd2, vv1);
            double b = compcoc(coordenada_x,xd,xd2,vv1);
            if(b < 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl; 
                cout <<"F(x): " << pendc <<"x " << b << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendc + b <<")"<< endl;
            }else if(b == 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl; 
                cout <<"F(x): " << pendc <<"x" << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendc + b <<")"<< endl;
            }else if(b > 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl; 
                cout <<"F(x): " << pendc <<"x +" << b << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendc + b <<")"<< endl;
            }else{
                cout << "Valor indeterminado" << endl;
            }
          	delete[] vv1.exponente; //delete[] (nombre del exponente) para borrar array.
         	delete[] vv1.constante;
          	delete[] vv1.exponente_c; //delete[] (nombre del exponente) para borrar array.
         	delete[] vv1.constante_c;
        	break;
        }
        case 3:
        {
        	cout << "Ingrese cantidad de elementos de f(x): " << endl;
            cin >> xd;
            variables vec = getExponenteConstante(xd,0,0);
              
            for(int w = 0; w < xd; w++){
              	if(vec.exponente[w] == 1){
                    cout << "+ " << vec.constante[w] << " ";
                }else if(vec.exponente[w] == 0){
                    cout << "";
                }else{
                    cout<< "+" << vec.constante[w]*vec.exponente[w] << "x^" << vec.exponente[w]-1 << " " ;
                }
            }
              
            delete[] vec.exponente; //delete[] (nombre del exponente) para borrar array.
         	delete[] vec.constante;
          	delete[] vec.exponente_c; //delete[] (nombre del exponente) para borrar array.
         	delete[] vec.constante_c;
        	break;
        }
        default:
        {
            cout << "Chao :p";
        }    
    }
    return 0;
}
