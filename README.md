Nombres:

-Andres Huiliñir, Rol: 202030503-k

-Juan Ignacio Chávez, Rol: 202030501-3

-Ferdinando Ambrosino, Rol: 202130550-5

Introducción: 
Las matematicas una tematica que ha existido desde tiempos antiguos, algo que para muchos se dificultan por lo complejo de algunas operaciones que se presentan y con el pasar del tiempo todos se han preguntado si hay alguna manera de faciclitarlas de algún modo. De ahí surgen las calculadoras, compactas y pueden ejecutar operaciones basicas, pero que pasa en los casos como las derivadas. Operaciones mas complejas y con incognitas que una calculadora común no puede resolver. Por eso optamos por la creación una calcualdora de derivadas, la cual entregue al usuario como resultado la derivada de una funcion, su recta tangente y su la pendeinte. En este proyecto incluimos su codificación, la  representacion grafica de la funcion resultante y la interfaz del programa.

Objetivo General: 
Realización de codigo el cual retorne en una calculadora de derivadas, que retorne su función derivada, recta tangente y su cociente.

Desarrollo: 
Para el codigo vamos a implementar los conocimientos entregados en los ramos de programación y de calculo de una variable. Del primero extraemos la utilizacion del lenguaje de programacion C++, para la codificacion de la calculadora y para la creacion de la interfaz y graficos. Vamos a utilizar un programa que se nos introdujo recientemente que es Qtcreator y Qtcharts. Qtcreator un programa utilizado por grandes empresas para la creacion de interfaces graficas y Qtcharts, que puede venir implementado en Qtcreator, este tiene como funcion el presentar graficos. Nos hemos apoyado en las clases particuladas en el ramo de TEL102. 

Implementando todo lo mencionado en una calculadora la cual pueda procesar ciertas funciones dentro del mismo programa, para despues retornar la funcion derivada, el grafico de la dreivada, una recta tangente y la pendiente. 

Esto lo realizaremos creando varias opciones dentro de la calculadora, haciendo que el usuario escoga la opcion que necesita realizar. Estas opciones serian:

-Derivada de la Funcion

-Derivada Recta Tangente

-Derivada con Cociente

-Salir (Si no se quiere utilizar)


Conclusión: 

Este fue un proyecto bastante entretenido y con mucho aprendizaje. Aprendimos una variedad inmensa de conocimentos de programacion, como de matematicas. En programación, logramos adaptarnos un poco al uso general de QT. La creacion de graficos e interfaces, la migracion de un codigo en un archivo cpp al qt. Ademas de el refuerzo a los aprendizaje de los lenguajes de programacion C/C++, por prueba y error logrando crear un codigo de una calidad buena y funcional.  
Agregando lo anterior, el reforzamiento de conocimientos de cálculo. Al hacer esto tuvimos que estudiar nuevamente toda la materia de derivadas y despues estudiar mas a fondo los casos que urilizamos para nuestra calculadora.


Diagrama de Componentes: https://drive.google.com/file/d/1jd4tgjzcWstkUWC6G6-YofNTUEHNPRnQ/view?usp=sharing

Fecha:  
Inicio de proyecto : 26 de Septiembre de 2021 
Término de proyecto : 27 de Diciembre de 2021

Requisitos de Compilacion: 

-ubuntu 20.04. vdi

-Se utiliza la terminal de Linux, utilizando el comando "g++".

-Verificación del paquete g++:
1. Abrir la terminal de linux utilizando la bandeja de aplicaciones o la combinacion de teclas que es "Ctrl+Alt+T".
2. Se utiliza el comando "g++ --Version", ahi se muestra la version que esta instalado en el computador.

Si resulta que no se tiene:
-Instalación:
1. Abrir la terminal de linux utilizando la bandeja de aplicaciones o la combinacion de teclas que es "Ctrl+Alt+T".
2. Utilizar le siguiente comando "sudo apt-get install g++" para iniciar la instalación.
3. Aprovar con "Y" las diversas peticiones para la instalación.

-Para los que tiene g++ desactualizado, utilizar el comando "sudo apt-update" para actualizarlo.

Fuente: https://www.enmimaquinafunciona.com/pregunta/78862/como-puedo-saber-que-tengo-g-instalado-en-mi-ubuntu


Referencia: [86-130; Earl W. Swokowski]
