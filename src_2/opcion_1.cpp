#include "opcion_1.h"
#include "ui_opcion_1.h"
#include <cmath>

void Opcion_1::CrearGraf(){
    ui->graphicsView->chart()->removeAllSeries();
    QLineSeries *series = new QLineSeries;
    //QLineSeries *series2 = new QLineSeries;
    for(int i = 0; i < 21;i++){
        int y = 0;
        y += ui->Const1->value()*ui->Exp1->value()*pow(i,ui->Exp1->value() - 1);
        y += ui->Const2->value()*ui->Exp2->value()*pow(i,ui->Exp2->value() - 1);
        series->append(i,y);
    }
    ui->graphicsView->chart()->addSeries(series);
}

Opcion_1::Opcion_1(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Opcion_1)
{
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(CrearGraf()));
}

Opcion_1::~Opcion_1()
{
    delete ui;
}
