#ifndef PROYECTOFINAL_H
#define PROYECTOFINAL_H
#include <QMainWindow>
#include <QtCharts/QLineSeries>

QT_BEGIN_NAMESPACE
namespace Ui { class ProyectoFinal; }
using namespace QtCharts;
QT_END_NAMESPACE

class ProyectoFinal : public QMainWindow
{
    Q_OBJECT

public:
    ProyectoFinal(QWidget *parent = nullptr);
    ~ProyectoFinal();
public slots:
    void GraficoTanNor();
    void GraficoTanDiv();
    void GrafDer();

private:
    Ui::ProyectoFinal *ui;
};
#endif // PROYECTOFINAL_H
