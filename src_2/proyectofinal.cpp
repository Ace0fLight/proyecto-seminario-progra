#include "proyectofinal.h"
#include "ui_proyectofinal.h"
#include "opcion_1.h"
#include <cmath>

void ProyectoFinal::GraficoTanNor(){
    ui->Denominador->hide();
    ui->Numerador->hide();
    ui->line->hide();
    ui->ConstD1->hide();
    ui->ConstD2->hide();
    ui->ConstD3->hide();
    ui->ConstD4->hide();
    ui->ConstD5->hide();
    ui->ConstD6->hide();
    ui->ExpD1->hide();
    ui->ExpD2->hide();
    ui->ExpD3->hide();
    ui->ExpD4->hide();
    ui->ExpD5->hide();
    ui->ExpD6->hide();
    ui->xd1->hide();
    ui->xd2->hide();
    ui->xd3->hide();
    ui->xd4->hide();
    ui->xd5->hide();
    ui->xd6->hide();
    ui->graphicsView->chart()->removeAllSeries();
    QLineSeries *recta = new QLineSeries;
    QLineSeries *Grafico = new QLineSeries;
    QPen pen1 = recta->pen();
    pen1.setWidth(1);
    pen1.setColor("red");
    recta->setPen(pen1);
    QPen pen2 = Grafico->pen();
    pen2.setWidth(1);
    pen2.setColor("black");
    recta->setPen(pen2);

    double y_recta = 0;
    double comp_y_recta = 0;
    y_recta += ui->Const1->value()*ui->Exp1->value()*pow(ui->PuntInt->value(),ui->Exp1->value()-1);
    comp_y_recta -= ui->PuntInt->value()*ui->Const1->value()*ui->Exp1->value()*pow(ui->PuntInt->value(),ui->Exp1->value()-1);
    comp_y_recta += ui->Const1->value()*pow(ui->PuntInt->value(),ui->Exp1->value());
    y_recta += ui->Const2->value()*ui->Exp2->value()*pow(ui->PuntInt->value(),ui->Exp2->value()-1);
    comp_y_recta -= ui->PuntInt->value()*ui->Const2->value()*ui->Exp2->value()*pow(ui->PuntInt->value(),ui->Exp2->value()-1);
    comp_y_recta += ui->Const2->value()*pow(ui->PuntInt->value(),ui->Exp2->value());
    y_recta += ui->Const3->value()*ui->Exp3->value()*pow(ui->PuntInt->value(),ui->Exp3->value()-1);
    comp_y_recta -= ui->PuntInt->value()*ui->Const3->value()*ui->Exp3->value()*pow(ui->PuntInt->value(),ui->Exp3->value()-1);
    comp_y_recta += ui->Const3->value()*pow(ui->PuntInt->value(),ui->Exp3->value());
    y_recta += ui->Const4->value()*ui->Exp4->value()*pow(ui->PuntInt->value(),ui->Exp4->value()-1);
    comp_y_recta -= ui->PuntInt->value()*ui->Const4->value()*ui->Exp4->value()*pow(ui->PuntInt->value(),ui->Exp4->value()-1);
    comp_y_recta += ui->Const4->value()*pow(ui->PuntInt->value(),ui->Exp4->value());
    y_recta += ui->Const5->value()*ui->Exp5->value()*pow(ui->PuntInt->value(),ui->Exp5->value()-1);
    comp_y_recta -= ui->PuntInt->value()*ui->Const5->value()*ui->Exp5->value()*pow(ui->PuntInt->value(),ui->Exp5->value()-1);
    comp_y_recta += ui->Const5->value()*pow(ui->PuntInt->value(),ui->Exp5->value());
    y_recta += ui->Const6->value()*ui->Exp6->value()*pow(ui->PuntInt->value(),ui->Exp6->value()-1);
    comp_y_recta -= ui->PuntInt->value()*ui->Const6->value()*ui->Exp6->value()*pow(ui->PuntInt->value(),ui->Exp6->value()-1);
    comp_y_recta += ui->Const6->value()*pow(ui->PuntInt->value(),ui->Exp6->value());

    for(double x = -15; x < 15; x+= 0.01){
        //grafico original
        double y = 0;
        y += ui->Const1->value()*pow(x,ui->Exp1->value());
        y += ui->Const2->value()*pow(x,ui->Exp2->value());
        y += ui->Const3->value()*pow(x,ui->Exp3->value());
        y += ui->Const4->value()*pow(x,ui->Exp4->value());
        y += ui->Const5->value()*pow(x,ui->Exp5->value());
        y += ui->Const6->value()*pow(x,ui->Exp6->value());
        if(y > 15){
            Grafico->append(x,15);
        }else if(y < -15){
            Grafico->append(x,-15);
        }else{
            Grafico->append(x,y);
        }
    }
    for(double x1 = -15;x1 < 15; x1+= 0.01){
        double y2 = x1 * y_recta + comp_y_recta;
        if(y2 > 15){
            recta->append(x1,15);
        }else if(y2 < -15){
            recta->append(x1,-15);
        }else{
            recta->append(x1,y2);
        }
    }
    //genera el grafico para la opcion 1 
    ui->graphicsView->chart()->addSeries(recta);
    ui->graphicsView->chart()->addSeries(Grafico);
    ui->graphicsView->chart()->createDefaultAxes();
    QString String1 = QString::number(y_recta);
    QString String2 = QString::number(comp_y_recta);
    ui->RectaTangenteLabel->show();
    ui->RectaTangenteLabel->setText("Recta Tangente: " + String1 + "x +" + String2);
}

void ProyectoFinal::GraficoTanDiv(){
    ui->Denominador->show();
    ui->Numerador->show();
    ui->line->show();
    ui->ConstD1->show();
    ui->ConstD2->show();
    ui->ConstD3->show();
    ui->ConstD4->show();
    ui->ConstD5->show();
    ui->ConstD6->show();
    ui->ExpD1->show();
    ui->ExpD2->show();
    ui->ExpD3->show();
    ui->ExpD4->show();
    ui->ExpD5->show();
    ui->ExpD6->show();
    ui->xd1->show();
    ui->xd2->show();
    ui->xd3->show();
    ui->xd4->show();
    ui->xd5->show();
    ui->xd6->show();

    ui->graphicsView->chart()->removeAllSeries();
    QLineSeries *Grafico = new QLineSeries;
    QLineSeries *Grafico2 = new QLineSeries;
    double fdelta = 0;
    double gdelta = 0;
    double efe = 0;
    double ge = 0;

    efe += ui->Const1->value()*pow(ui->PuntInt->value(),ui->Exp1->value());
    efe += ui->Const2->value()*pow(ui->PuntInt->value(),ui->Exp2->value());
    efe += ui->Const3->value()*pow(ui->PuntInt->value(),ui->Exp3->value());
    efe += ui->Const4->value()*pow(ui->PuntInt->value(),ui->Exp4->value());
    efe += ui->Const5->value()*pow(ui->PuntInt->value(),ui->Exp5->value());
    efe += ui->Const6->value()*pow(ui->PuntInt->value(),ui->Exp6->value());
    fdelta += ui->Const1->value()*ui->Exp1->value()*pow(ui->PuntInt->value(),ui->Exp1->value()-1);
    fdelta += ui->Const2->value()*ui->Exp2->value()*pow(ui->PuntInt->value(),ui->Exp2->value()-1);
    fdelta += ui->Const3->value()*ui->Exp3->value()*pow(ui->PuntInt->value(),ui->Exp3->value()-1);
    fdelta += ui->Const4->value()*ui->Exp4->value()*pow(ui->PuntInt->value(),ui->Exp4->value()-1);
    fdelta += ui->Const5->value()*ui->Exp5->value()*pow(ui->PuntInt->value(),ui->Exp5->value()-1);
    fdelta += ui->Const6->value()*ui->Exp6->value()*pow(ui->PuntInt->value(),ui->Exp6->value()-1);
    ge += ui->ConstD1->value()*pow(ui->PuntInt->value(),ui->ExpD1->value());
    ge += ui->ConstD2->value()*pow(ui->PuntInt->value(),ui->ExpD2->value());
    ge += ui->ConstD3->value()*pow(ui->PuntInt->value(),ui->ExpD3->value());
    ge += ui->ConstD4->value()*pow(ui->PuntInt->value(),ui->ExpD4->value());
    ge += ui->ConstD5->value()*pow(ui->PuntInt->value(),ui->ExpD5->value());
    ge += ui->ConstD6->value()*pow(ui->PuntInt->value(),ui->ExpD6->value());
    gdelta += ui->ConstD1->value()*ui->ExpD1->value()*pow(ui->PuntInt->value(),ui->ExpD1->value()-1);
    gdelta += ui->ConstD2->value()*ui->ExpD2->value()*pow(ui->PuntInt->value(),ui->ExpD2->value()-1);
    gdelta += ui->ConstD3->value()*ui->ExpD3->value()*pow(ui->PuntInt->value(),ui->ExpD3->value()-1);
    gdelta += ui->ConstD4->value()*ui->ExpD4->value()*pow(ui->PuntInt->value(),ui->ExpD4->value()-1);
    gdelta += ui->ConstD5->value()*ui->ExpD5->value()*pow(ui->PuntInt->value(),ui->ExpD5->value()-1);
    gdelta += ui->ConstD6->value()*ui->ExpD6->value()*pow(ui->PuntInt->value(),ui->ExpD6->value()-1);

    double pendiente_cociente = (fdelta * ge - gdelta * efe)/pow(ge,2);
    double comp_cociente = (pendiente_cociente*ui->PuntInt->value()*(-1)) + (efe/ge);


    for(double x = -15; x < 15; x+= 0.01){
        //grafico 1
        double y = 0;
        y += ui->Const1->value()*pow(x,ui->Exp1->value());
        y += ui->Const2->value()*pow(x,ui->Exp2->value());
        y += ui->Const3->value()*pow(x,ui->Exp3->value());
        y += ui->Const4->value()*pow(x,ui->Exp4->value());
        y += ui->Const5->value()*pow(x,ui->Exp5->value());
        y += ui->Const6->value()*pow(x,ui->Exp6->value());

        double yy = 0;
        yy += ui->ConstD1->value()*pow(x,ui->ExpD1->value());
        yy += ui->ConstD2->value()*pow(x,ui->ExpD2->value());
        yy += ui->ConstD3->value()*pow(x,ui->ExpD3->value());
        yy += ui->ConstD4->value()*pow(x,ui->ExpD4->value());
        yy += ui->ConstD5->value()*pow(x,ui->ExpD5->value());
        yy += ui->ConstD6->value()*pow(x,ui->ExpD6->value());

        if(y/yy > 15){
            Grafico->append(x,15);
        }else if(y/yy < -15){
            Grafico->append(x,-15);
        }else{
            Grafico->append(x,y/yy);
        }
    }
    for(double x=-15; x<15; x+=0.01){
        //grafico 2
        if((x*pendiente_cociente+comp_cociente) < -15){
            Grafico2->append(x,-15);
        }
        else if ((x*pendiente_cociente+comp_cociente) > 15){
            Grafico2->append(x,15);
        }
        else{
            Grafico2->append(x,(x*pendiente_cociente+comp_cociente));}

    }
    //genera el grafico para la opcion 2
    ui->graphicsView->chart()->addSeries(Grafico);
    ui->graphicsView->chart()->addSeries(Grafico2);
    ui->graphicsView->chart()->createDefaultAxes();
    QString String1 = QString::number(pendiente_cociente);
    QString String2 = QString::number(comp_cociente);
    ui->RectaTangenteLabel->show();
    ui->RectaTangenteLabel->setText("Recta Tangente: " + String1 + "x +" + String2);
}

void ProyectoFinal::GrafDer(){
    ui->Denominador->hide();
    ui->Numerador->hide();
    ui->line->hide();
    ui->ConstD1->hide();
    ui->ConstD2->hide();
    ui->ConstD3->hide();
    ui->ConstD4->hide();
    ui->ConstD5->hide();
    ui->ConstD6->hide();
    ui->ExpD1->hide();
    ui->ExpD2->hide();
    ui->ExpD3->hide();
    ui->ExpD4->hide();
    ui->ExpD5->hide();
    ui->ExpD6->hide();
    ui->xd1->hide();
    ui->xd2->hide();
    ui->xd3->hide();
    ui->xd4->hide();
    ui->xd5->hide();
    ui->xd6->hide();
    ui->graphicsView->chart()->removeAllSeries();
    QLineSeries *Grafico = new QLineSeries;
    for(double x = -15; x < 15; x += 0.01){
        double y = 0;
        y += ui->Const1->value()*ui->Exp1->value()*pow(x,ui->Exp1->value() -1);
        y += ui->Const2->value()*ui->Exp2->value()*pow(x,ui->Exp2->value() -1);
        y += ui->Const3->value()*ui->Exp3->value()*pow(x,ui->Exp3->value() -1);
        y += ui->Const4->value()*ui->Exp4->value()*pow(x,ui->Exp4->value() -1);
        y += ui->Const5->value()*ui->Exp5->value()*pow(x,ui->Exp5->value() -1);
        y += ui->Const6->value()*ui->Exp6->value()*pow(x,ui->Exp6->value() -1);

        if(y > 15){
            Grafico->append(x,15);
        }else if(y<-15){
            Grafico->append(x,-15);
        }else{
            Grafico->append(x,y);
        }
    }
    //genera el grafico para la opcion 3
    ui->graphicsView->chart()->addSeries(Grafico);
    ui->graphicsView->chart()->createDefaultAxes();
    ui->RectaTangenteLabel->show();
    ui->RectaTangenteLabel->setText(QString::number(ui->Const1->value()*ui->Exp1->value()) + "^(" + QString::number(ui->Exp1->value() - 1) +")" + " + " + QString::number(ui->Const2->value()*ui->Exp2->value()) + "^(" + QString::number(ui->Exp2->value() - 1) +")" + " + " + QString::number(ui->Const3->value()*ui->Exp3->value()) + "^(" + QString::number(ui->Exp3->value() - 1) +")" + " + " + QString::number(ui->Const4->value()*ui->Exp4->value()) + "^(" + QString::number(ui->Exp4->value() - 1) +")" + " + " + QString::number(ui->Const5->value()*ui->Exp5->value()) + "^(" + QString::number(ui->Exp5->value() - 1) +")" + " + " + QString::number(ui->Const6->value()*ui->Exp6->value()) + "^(" + QString::number(ui->Exp6->value() - 1) +")");

}

ProyectoFinal::ProyectoFinal(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ProyectoFinal)
{
    ui->setupUi(this);
    connect(ui->BotTanNor,SIGNAL(clicked()),this,SLOT(GraficoTanNor()));
    connect(ui->BotTanDiv,SIGNAL(clicked()),this,SLOT(GraficoTanDiv()));
    connect(ui->BotDer,SIGNAL(clicked()),this,SLOT(GrafDer()));
    ui->RectaTangenteLabel->hide();
    ui->Denominador->hide();
    ui->Numerador->hide();
    ui->line->hide();
    ui->ConstD1->hide();
    ui->ConstD2->hide();
    ui->ConstD3->hide();
    ui->ConstD4->hide();
    ui->ConstD5->hide();
    ui->ConstD6->hide();
    ui->ExpD1->hide();
    ui->ExpD2->hide();
    ui->ExpD3->hide();
    ui->ExpD4->hide();
    ui->ExpD5->hide();
    ui->ExpD6->hide();
    ui->xd1->hide();
    ui->xd2->hide();
    ui->xd3->hide();
    ui->xd4->hide();
    ui->xd5->hide();
    ui->xd6->hide();
}

ProyectoFinal::~ProyectoFinal()
{
    delete ui;

}
