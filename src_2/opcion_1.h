#ifndef OPCION_1_H
#define OPCION_1_H

#include <QWidget>
#include <QtCharts>

namespace Ui {
class Opcion_1;
}
using namespace QtCharts;
class Opcion_1 : public QWidget
{
    Q_OBJECT

public:
    explicit Opcion_1(QWidget *parent = nullptr);
    ~Opcion_1();
    void CrearGraf();

private:    Ui::Opcion_1 *ui;
};

#endif // OPCION_1_H
