#ifndef archivoh_H
#define archivoh_H
#include "funciones.cpp"
#include <QtCharts/QLineSeries>

variables getExponenteConstante(int n,int n2, int boton);
double pendiente(double x,int n, variables b);
double CompB(double ubicacion,int camino, variables bloque);
double cociente(double x, int n1, int n2, variables estructura);
double compcoc(double x, int n1, int n2, variables estructura);

#endif
