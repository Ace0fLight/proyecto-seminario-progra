#include <QApplication>
#include <cmath>
#include <iostream>
#include "archivoh.h"
#include <QtCharts>
using namespace std;
struct variables{
    double *exponente;
    double *constante; 
    double *exponente_c;
    double *constante_c;
};

variables getExponenteConstante(int n,int n2, int boton){ //"0" de "no cociente" y "1" de "si cociente"
	variables v;
    v.exponente = new double[n];
    v.constante = new double[n];
    v.exponente_c = new double[n2];
    v.constante_c = new double[n2];
	switch(boton){
        case 0:
            int i;
            for (i = 0; i < n; i++){
                cout << "Ingrese el Exponente de la variable X: " << endl;
                cin >> v.exponente[i];
                cout << "Ingrese el valor de la constante: " << endl;
                cin >> v.constante[i];
            }
            break;
        case 1:
            cout << "Numerador" << endl;
            int ii;
            for (ii = 0; ii < n; ii++){
                cout << "Ingrese el Exponente de la variable X: " << endl;
                cin >> v.exponente[ii];
                cout << "Ingrese el valor de la constante: " << endl;
                cin >> v.constante[ii];
            }
            cout << "Denominador" << endl;

            for(int j = 0; j < n2; j++){
                cout << "Ingrese el Exponente de la variable X: " << endl;
                cin >> v.exponente_c[j];
                cout << "Ingrese el valor de la constante: " << endl;
                cin >> v.constante_c[j];
            
            }
            break;
    }
    return v;
}

// Y = [f´(x1)*X] [-f´(x1)*X1 + f(x1)] = mx + b
//derivada
double pendiente(double x,int n, variables b){

    double contador = 0;
    for(int i = 0; i < n; i++){
        if(x == 0 && (b.exponente[i] == 1 || b.exponente[i] == 0)){
            contador += 0;
        }else{
            contador += b.constante[i]*b.exponente[i]*pow(x,b.exponente[i]-1);
        }
    }
    return contador;
}
//cambiar "ultimate" a "CompB"
double CompB(double ubicacion,int camino, variables bloque){
    double elmensajero = 0;
    for(int registro = 0; registro < camino; registro++){
        if(ubicacion == 0 && (bloque.exponente[registro] == 1 || bloque.exponente[registro] == 0)){
            elmensajero -= bloque.constante[registro]*bloque.exponente[registro]; //dev
            elmensajero += bloque.constante[registro]; //normal            
        }else{
            elmensajero -= ubicacion*bloque.constante[registro]*bloque.exponente[registro]*pow(ubicacion,bloque.exponente[registro]-1); //dev
            elmensajero += bloque.constante[registro]*pow(ubicacion,bloque.exponente[registro]); //normal
        }
    }
    return elmensajero;
}

double cociente(double x, int n1, int n2, variables estructura){
    double f = 0,fd = 0,g = 0,gd = 0;
    double retorno; 
    for(int i = 0; i < n1; i++){
        f += estructura.constante[i]*pow(x,estructura.exponente[i]);

        if(x == 0 && (estructura.exponente[i] == 1 || estructura.exponente[i] == 0)){
            fd += 0;
        }else{
            fd += estructura.constante[i]*estructura.exponente[i]*pow(x,estructura.exponente[i]-1);
        }
    }

    for(int t = 0; t < n2; t++){
        g += estructura.constante_c[t]*pow(x,estructura.exponente_c[t]);

        if(x == 0 && (estructura.exponente_c[t] == 1 || estructura.exponente_c[t] == 0)){
            gd += 0;
        }else{
            gd += estructura.constante_c[t]*estructura.exponente_c[t]*pow(x,estructura.exponente_c[t]-1);
        }
    }
    retorno = (fd*g - gd*f)/pow(g,2);
    return retorno;
}

double compcoc(double x, int n1, int n2, variables estructura){

    double parte_1 = cociente(x,n1,n2,estructura)*(-1)*x;
    double cx = 0;
    double xc = 0;
    double parte_2;
    for(int z = 0; z < n1; z++){
        if(x == 0 && (estructura.exponente[z] == 0)){
            cx += estructura.constante[z];
        }else{
            cx += estructura.constante[z]*pow(x,estructura.exponente[z]);
        }
    }
    for(int y = 0; y < n2; y++){
        if(x == 0 && (estructura.exponente_c[y] == 0)){
            xc += estructura.constante_c[y];
        }else{
            xc += estructura.constante_c[y]*pow(x,estructura.exponente_c[y]);
        }
    }
    parte_2 = (cx/xc) + parte_1;
    return parte_2;
}
