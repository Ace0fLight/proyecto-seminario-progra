#include "proyectotel102.h"
#include "archivoh.h"
#include <QApplication>
#include <QtCharts>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    proyectotel102 w;
    QLineSeries *series = new QLineSeries();
    QLineSeries *series2 = new QLineSeries();
    int xd, xd2, opc;
    double coordenada_x;

    cout << "Bienvenidos a la Calculadora de Derivadas orientadas a funciones made in USM" << endl;
    cout << "Porfavor ingrese una opcion" << endl;
    cout << "Opcion 1: Recta Tangente de la Funcion " << endl;
    cout << "Opcion 2: Recta Tangente con cociente" << endl;
    cout << "Opcion 3: La Derivada de la Funcion Corriente" << endl;
    cout << "Opcion 4: Salir." << endl;
    cin >> opc;

    switch(opc){
        case 1:
        {
            cout << "Ingrese cantidad de elementos de f(x): " << endl;
            cin >> xd;
            variables vv = getExponenteConstante(xd,0,0);
            cout << "Ingrese coordenada x: ";
            cin >> coordenada_x;

            if(CompB(coordenada_x,xd,vv) < 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl;
                cout <<"F(x): " << pendiente(coordenada_x,xd,vv) <<"x " << CompB(coordenada_x,xd,vv) << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendiente(coordenada_x,xd,vv) + CompB(coordenada_x,xd,vv) <<")"<< endl;
            }else if(CompB(coordenada_x,xd,vv) == 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl;
                cout <<"F(x): " << pendiente(coordenada_x,xd,vv) <<"x" << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendiente(coordenada_x,xd,vv) + CompB(coordenada_x,xd,vv) <<")"<< endl;
            }else if(CompB(coordenada_x,xd,vv) > 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl;
                cout <<"F(x): " << pendiente(coordenada_x,xd,vv) <<"x +" << CompB(coordenada_x,xd,vv) << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendiente(coordenada_x,xd,vv) + CompB(coordenada_x,xd,vv) <<")"<< endl;
            }else{
                cout << "Valor indeterminado" << endl;
            }

            for(float x2 = - 15.0; x2 < 15; x2 += 0.1){
                double yy = 0;
                double y = x2*pendiente(coordenada_x,xd,vv)+CompB(coordenada_x,xd,vv);
                series->append(x2,y);
                for(int x = 0; x < xd; x++){
                    yy += vv.constante[x]*pow(x2,vv.exponente[x]);
                }
                series2->append(x2,yy);

            }
            delete[] vv.exponente; //delete[] (nombre del exponente) para borrar array.
            delete[] vv.constante;
            delete[] vv.exponente_c; //delete[] (nombre del exponente) para borrar array.
            delete[] vv.constante_c;
            break;
        }
        case 2:
        {
            cout << "Ingrese cantidad de elementos del numerador de f(x): " << endl;
            cin >> xd;
            cout << "Ingrese cantidad de elementos del denominador de f(x): " << endl;
            cin >> xd2;
            variables vv1 = getExponenteConstante(xd,xd2,1);

            cout << "Ingrese coordenada x: ";
            cin >> coordenada_x;
            double pendc = cociente(coordenada_x, xd, xd2, vv1);
            double b = compcoc(coordenada_x,xd,xd2,vv1);
            if(b < 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl;
                cout <<"F(x): " << pendc <<"x " << b << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendc + b <<")"<< endl;
            }else if(b == 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl;
                cout <<"F(x): " << pendc <<"x" << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendc + b <<")"<< endl;
            }else if(b > 0){
                cout <<"Recta tangente en la posicion "<< coordenada_x << endl;
                cout <<"F(x): " << pendc <<"x +" << b << endl;
                cout <<"Intersecta en la coordenada (" << coordenada_x << "," << coordenada_x*pendc + b <<")"<< endl;
            }else{
                cout << "Valor indeterminado" << endl;
            }
            for(float x1 = -50.0; x1 < 50.0;x1 += 0.1){
                double y = 0, yc = 0;
                for(int y1 = 0; y1 < xd; y1++){
                    y += vv1.constante[y1]*pow(x1,vv1.exponente[y1]);
                }
                for(int y2 = 0; y2 < xd2; y2++){
                    yc += vv1.constante_c[y2]*pow(x1,vv1.exponente_c[y2]);
                }
                series2->append(x1,x1*pendc + b);

                if(50 <= y/yc){
                    series->append(x1,50);
                }else if(y/yc <= -50){
                    series->append(x1,-50);
                }else if(yc == 0){
                    break;
                }else{
                    series->append(x1,y/yc);
                }
            }
            delete[] vv1.exponente; //delete[] (nombre del exponente) para borrar array.
            delete[] vv1.constante;
            delete[] vv1.exponente_c; //delete[] (nombre del exponente) para borrar array.
            delete[] vv1.constante_c;
            break;
        }
        case 3:
        {
            cout << "Ingrese cantidad de elementos de f(x): " << endl;
            cin >> xd;
            variables vec = getExponenteConstante(xd,0,0);

            for(int w = 0; w < xd; w++){
                if(vec.exponente[w] == 1){
                    cout << "+ " << vec.constante[w] << " ";
                }else if(vec.exponente[w] == 0){
                    cout << "";
                }else{
                    cout<< "+" << vec.constante[w]*vec.exponente[w] << "x^" << vec.exponente[w]-1 << " " ;
                }
            }
            for(float x1 = -15.0; x1 < 15.0;x1 += 0.1){
                double y = 0;
                for(int y1 = 0; y1 < xd; y1++){
                    if(x1 == 0 && (vec.exponente[y1] == 1 || vec.exponente[y1] == 0)){
                        y += 0;
                    }else{
                        y += vec.constante[y1]*vec.exponente[y1]*pow(x1,vec.exponente[y1]-1);
                    }
                }
                series->append(x1,y);
            }

            delete[] vec.exponente; //delete[] (nombre del exponente) para borrar array.
            delete[] vec.constante;
            delete[] vec.exponente_c; //delete[] (nombre del exponente) para borrar array.
            delete[] vec.constante_c;
            break;
        }
        default:
        {
            cout << "Chao :p";
        }
    }
    QChart *chart = new QChart();

    chart->addSeries(series);
    chart->addSeries(series2);
    chart->legend()->hide();
    chart->createDefaultAxes();
    QChartView *chartView = new QChartView(chart);
    w.setCentralWidget(chartView);
    w.resize(420,300);
    w.show();
    return a.exec();
}
