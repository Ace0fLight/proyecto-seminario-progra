#ifndef PROYECTOTEL102_H
#define PROYECTOTEL102_H
#include <iostream>
#include <QMainWindow>
#include <QtCharts/QLineSeries>

QT_BEGIN_NAMESPACE
namespace Ui { class proyectotel102; }
using namespace QtCharts;
QT_END_NAMESPACE

class proyectotel102 : public QMainWindow
{
    Q_OBJECT

public:
    proyectotel102(QWidget *parent = nullptr);
    ~proyectotel102();

private:
    Ui::proyectotel102 *ui;
};
#endif // PROYECTOTEL102_H
